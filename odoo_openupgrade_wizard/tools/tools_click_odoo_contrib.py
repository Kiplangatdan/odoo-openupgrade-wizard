import shutil

from loguru import logger

from odoo_openupgrade_wizard.tools.tools_postgres import (
    ensure_database,
    execute_sql_request,
)


def copydb(ctx, source, dest):
    # drop database if exist
    ensure_database(ctx, dest, state="absent")

    # Copy database
    logger.info(f"Copy database {source} into {dest} ...")
    request = f"CREATE DATABASE {dest} WITH TEMPLATE {source};"
    execute_sql_request(ctx, request)

    main_path = ctx.obj["filestore_folder_path"] / "filestore"
    source_path = main_path / source
    dest_path = main_path / dest
    # Drop filestore if exist
    logger.info(f"Remove filestore of '{dest}' if exists.")
    shutil.rmtree(dest_path, ignore_errors=True)

    # Copy Filestore
    logger.info(f"Copy filestore of '{source}' into '{dest}' folder ...")
    shutil.copytree(source_path, dest_path)
