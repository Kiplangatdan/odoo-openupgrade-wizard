import pathlib
import shutil

from odoo_openupgrade_wizard.tools.tools_postgres import ensure_database

from . import (
    build_ctx_from_config_file,
    cli_runner_invoke,
    move_to_test_folder,
)


def test_cli_copydb():
    move_to_test_folder()

    db_name = "database_test_cli___copydb"
    db_dest_name = "database_test_cli___copydb__copy"
    ctx = build_ctx_from_config_file()

    # Ensure environment is clean
    ensure_database(ctx, db_name, state="absent")
    dest_filestore_path = pathlib.Path(f"./filestore/filestore/{db_dest_name}")
    shutil.rmtree(dest_filestore_path, ignore_errors=True)

    # Initialize database
    cli_runner_invoke(
        [
            "--log-level=DEBUG",
            "install-from-csv",
            f"--database={db_name}",
        ],
    )

    # Copy database
    cli_runner_invoke(
        [
            "--log-level=DEBUG",
            "copydb",
            f"--source={db_name}",
            f"--dest={db_dest_name}",
        ],
    )

    # check filestore exists
    assert dest_filestore_path.exists()

    # Delete filestore
    shutil.rmtree(dest_filestore_path)
